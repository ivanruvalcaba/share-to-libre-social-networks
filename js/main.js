/*
 * File: main.js
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the GNU General Public
 * License v3. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://www.gnu.org/licenses/gpl-3.0.en.html.
 */

// Set pop-up window properties.
var height = 500;
var width = 650;
var left = Number((screen.width / 2) - (width / 2));
var top = Number((screen.height / 2) - (height / 2));

$(document).ready(function () {
  // Get url parameter.
  var sURL = window.location;
  var params = _.chain(sURL).split('?').nth(1).split('&')
    .map(_.partial(_.split, _, '=', 2))
    .fromPairs().value();

  $('.text').val(decodeURIComponent(params['text']));
  $('.url').val(decodeURIComponent(params['url']));

  // Get browser local storage.
  var hubzilla = localStorage.getItem('Hubzilla');
  var diaspora = localStorage.getItem('Diaspora');
  var gnusocial = localStorage.getItem('Gnusocial');
  var mastodon = localStorage.getItem('Mastodon');

  if (hubzilla !== null) {
    $('#hostHubzilla').val(hubzilla);
    $('.hubzilla').removeAttr('disabled');
  } else if (diaspora !== null) {
    $('#hostDiaspora').val(diaspora);
    $('.diaspora').removeAttr('disabled');
  } else if (gnusocial !== null) {
    $('#hostGnusocial').val(gnusocial);
    $('.gnusocial').removeAttr('disabled');
  } else if (mastodon !== null) {
    $('#hostMastodon').val(mastodon);
    $('.mastodon').removeAttr('disabled');
  }

  $('.host').keyup(function () {
    var service = $(this).closest("form").attr('data-service');

    if (service === 'Hubzilla') {
      $('.hubzilla').removeAttr('disabled');
    } else if (service === 'Diaspora') {
      $('.diaspora').removeAttr('disabled');
    } else if (service === 'Gnusocial') {
      $('.gnusocial').removeAttr('disabled');
    } else if (service === 'Mastodon') {
      $('.mastodon').removeAttr('disabled');
    }
  });

  // Open new pop-up window.
  $('.send').click(function (e) {
    e.preventDefault();

    var site = $(this).attr('data-site');
    var check = $('#remember' + site);

    if (check.is(':checked')) {
      var host = $('#host' + site).val();
      localStorage.setItem(site, host);
    }

    var text = $('#text' + site).val();
    var host = $('#host' + site).val();
    var url = $('#url' + site).val();
    var action = ' ';
    var service = $(this).closest("form").attr('data-service');

    if (host !== '' && text !== '') {
      // Add protocol https
      var prefix = 'https://';

      if (host.substr(0, prefix.length) !== prefix) {
        host = prefix + host;
      }

      if (service === 'Hubzilla') {
        action = host + '/rpost?body=' + encodeURIComponent(text) +
          '&url=' + encodeURIComponent(url);
        $('.hubzilla').removeAttr('disabled');
      } else if (service === 'Diaspora') {
        action = host + '/bookmarklet?url=' + encodeURIComponent(url) +
          '&title=' + encodeURIComponent(text) + '&jump=doclose';
        $('.diaspora').removeAttr('disabled');
      } else if (service === 'Gnusocial') {
        action = host + '/?action=newnotice&status_textarea=' +
          encodeURIComponent(text) + ' ' + encodeURIComponent(url);
        $('.gnusocial').removeAttr('disabled');
      } else if (service === 'Mastodon') {
        action = host + '/share?text=' + encodeURIComponent(text) +
          '&url=' + encodeURIComponent(url);
        $('.mastodon').removeAttr('disabled');
      }
    }

    window.open(action, '_blank', 'width=' + width + ',height=' +
      height + ',left=' + left + ',top=' + top + '');
  });
});
