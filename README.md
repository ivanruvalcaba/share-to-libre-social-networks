# share-to-libre-social-networks

![](https://victorhckinthefreeworld.files.wordpress.com/2018/04/share_freedom.png)

Código para compartir contenidos de webs o blogs en redes sociales libres y federadas como Mastodon, Diaspora, GNU Social o Hubzilla.

## Referencias

Para mayor información: [https://victorhckinthefreeworld.com/2018/04/06/como-anadir-un-boton-en-tu-wordpress-para-compartir-contenido-en-mastodon-diaspora-gnusocial-o-hubzilla/](https://victorhckinthefreeworld.com/2018/04/06/como-anadir-un-boton-en-tu-wordpress-para-compartir-contenido-en-mastodon-diaspora-gnusocial-o-hubzilla/).

## Licencia

This project is subject to the terms of the [GNU General Public License, Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html).
